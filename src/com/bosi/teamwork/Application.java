package com.bosi.teamwork;

import com.bosi.teamwork.service.Biz;

/**
 * @author hcf1
 * @date 2021/12/2 16:06
 * @description TODO
 */
public class Application {
    public static void main(String[] args) {
        Biz.a1();
        Biz.b1();
        Biz.c1();
        Biz.d1();
        Biz.a2();
        Biz.b2();
        Biz.c2();
        Biz.d2();
    }
}
