package com.bosi.teamwork.service;

/**
 * @author hcf1
 * @date 2021/12/2 16:07
 * @description TODO
 */
public class Biz {

    public static void a1() {
        System.out.print("月黑剑光明,");
    }

    public static void b1() {
        System.out.println("横风匹马行。");
    }

    public static void c1() {
        System.out.print("雪收天地色,");
    }

    public static void d1(){
        System.out.println("冰压泲河声。");
    }

    public static void a2() {
        System.out.print("月黑剑光明,");
    }

    public static void b2(){
        System.out.println("回舟候雁鸣。");
    }

    public static void c2() {
        System.out.print("孤游惜奇险,");
    }
    
    public static void d2(){
        System.out.println("飞鞚过齐城。");
    }

}
